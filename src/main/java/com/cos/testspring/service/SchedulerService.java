package com.cos.testspring.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.LocalDateTime;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.cos.testspring.model.Hydrogen;
import com.cos.testspring.repository.HydrogenRepository;
import com.cos.testspring.web.api.ApiController;
import com.cos.testspring.web.dto.SocketDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class SchedulerService {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    private final HydrogenRepository hydrogenRepository;
    private final ApiController apiController;
    static boolean aaaa =true;//첫 작용 여부 확인
    @Scheduled(fixedDelay = 1000) // 1분마다 실행 1초 = 1000
    public void run() {
        BufferedReader in = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            
            if(aaaa){
                aaaa=false;
                if(true){
                URL obj = new URL("http://el.h2nbiz.or.kr/api/chrstnList/operationInfo"); // 호출할 url
                HttpURLConnection con = (HttpURLConnection)obj.openConnection();
     
                con.setRequestMethod("GET");//get요청
                //헤더값
                con.setRequestProperty("Authorization", "sMVYBl1av5YMSl3m0COY3VL87X3SalmUTf2qrVqlJEd0aQVfwXGFUVz3q414RD27");
                //요청한후 리턴값을 UTF-8(한글 가능)으로 값을 가져옴
                in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
                //가져온값을 JSONArray에 담아줌
                JSONArray jArray = new JSONArray(in.readLine());
                
                for(int i =0;i<jArray.length();i++){
                    //제이슨 데이터를 Hydrogen테이블에 담아줌
                    Hydrogen objectUser = objectMapper.readValue(jArray.get(i).toString(), Hydrogen.class);
                    objectUser.setTimestamp1(LocalDateTime.now());
                    hydrogenRepository.save(objectUser);//db에 저장
                }
            }
            URL obj = new URL("http://el.h2nbiz.or.kr/api/chrstnList/currentInfo"); // 호출할 url
                HttpURLConnection con = (HttpURLConnection)obj.openConnection();
     
                con.setRequestMethod("GET");
                con.setRequestProperty("Authorization", "sMVYBl1av5YMSl3m0COY3VL87X3SalmUTf2qrVqlJEd0aQVfwXGFUVz3q414RD27");
     
                in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
    
                JSONArray jArray = new JSONArray(in.readLine());
                
                for(int i =0;i<jArray.length();i++){
                    //가져온 데이터의 아이디로 기존에 있던 테이블을 가져와서 데이터를 넣어줌
                    Hydrogen newHydrogen = objectMapper.readValue(jArray.get(i).toString(), Hydrogen.class);
                    Hydrogen hydrogen = hydrogenRepository.findById(newHydrogen.getChrstn_mno()).orElseThrow();
                    hydrogen.set(newHydrogen);
                    hydrogenRepository.save(hydrogen);
                }
            }
            //웹소켓에 신호를 정해진 시간마다 보내줌
            simpMessagingTemplate.convertAndSend("/hydrogen/public",apiController.getHydrogen5());
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if(in != null) try { in.close(); } catch(Exception e) { e.printStackTrace(); }
        }
    }
}
