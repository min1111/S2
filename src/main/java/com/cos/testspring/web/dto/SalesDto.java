package com.cos.testspring.web.dto;

import java.util.ArrayList;
import java.util.List;

import com.cos.testspring.model.Sales;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor//모든 변수에 값을 넣는 생성자
@NoArgsConstructor//빈 생성자
@Data//getter,setter생성
public class SalesDto{
    private String date;
	private int quantity;
    private String id;
	private int targetValue;
    private double value;
    private String name;
    
    public SalesDto(String date,
	 int quantity,
     String id,
	 int targetValue,
     double value){
        this.date=date;
        this.quantity=quantity;
        this.id=id;
        this.targetValue=targetValue;
        this.value=value;
    }
}