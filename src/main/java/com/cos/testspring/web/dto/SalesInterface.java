package com.cos.testspring.web.dto;

public interface SalesInterface {
    String getDate();
    Integer getQuantity();
    String getName();
}
