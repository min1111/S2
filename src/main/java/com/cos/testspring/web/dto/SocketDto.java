package com.cos.testspring.web.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class SocketDto<T> {
	
	private int code; //1(성공), -1(실패)
	private int count;
	private T result;
}

