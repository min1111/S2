package com.cos.testspring.web.dto;

public interface HydrogenSensorInterface {
    Integer getSum();
}
