package com.cos.testspring.web.dto;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cos.testspring.model.Board;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserDto {

	private String username;
    private String password;
	private String passwordCheck;
    private String phone;
	private String role;
	
}
