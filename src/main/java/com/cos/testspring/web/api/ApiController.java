package com.cos.testspring.web.api;

import com.cos.testspring.config.auth.PrincipalDetails;
import com.cos.testspring.config.constants.Constants;
import com.cos.testspring.model.Hydrogen;
import com.cos.testspring.model.HydrogenSensor;
import com.cos.testspring.model.Message;
import com.cos.testspring.model.User;
import com.cos.testspring.repository.BoardRepository;
import com.cos.testspring.repository.HydrogenRepository;
import com.cos.testspring.repository.HydrogenSensorRepository;
import com.cos.testspring.repository.MessageRepository;
import com.cos.testspring.repository.SalesRepository;
import com.cos.testspring.repository.UserRepository;
import com.cos.testspring.service.BoardService;
import com.cos.testspring.service.SalesService;
import com.cos.testspring.web.dto.BoardDto;
import com.cos.testspring.web.dto.CMRespDto;
import com.cos.testspring.web.dto.SocketDto;
import com.cos.testspring.web.dto.UserDto;
import com.cos.testspring.web.dto.Excel;
import com.cos.testspring.web.dto.HydrogenSensorInterface;
import com.cos.testspring.web.dto.loginBody;
import lombok.RequiredArgsConstructor;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@RequiredArgsConstructor
@RestController
public class ApiController {
    public Constants constants;
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final BoardService boardService;
    private final SalesService salesService;
    private final BoardRepository boardRepository;
    private final SalesRepository salesRepository;
    private final MessageRepository messageRepository;
    private final HydrogenRepository hydrogenRepository;
    private final HydrogenSensorRepository hydrogenSensorRepository;
    
    public boolean newTokenCheck = false;

    public void setNewTokenCheck(boolean newTokenCheck){
        this.newTokenCheck=newTokenCheck;
    }
    
    @PostMapping("/join")
    public @ResponseBody ResponseEntity<?> join(User user){
        System.out.println("----------");
        System.out.println(user);
        System.out.println(user.getEmail());
        System.out.println("----------");
        if(userRepository.findByEmail(user.getEmail()) != null){
            System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
            return new ResponseEntity<>(new CMRespDto<>(-1,"이미 존재하는 계정입니다.",null,null,new loginBody()),HttpStatus.OK);
        }

        user.setRole("ROLE_USER");

        String rawPassword = user.getPassword();
        String encPassword = bCryptPasswordEncoder.encode(rawPassword);

        user.setUsername(user.getUsername());
        user.setPassword(encPassword);
        user.setProfil("3.jpg");

        userRepository.save(user);
        return new ResponseEntity<>(new CMRespDto<>(1,"회원가입성공",null,null,new loginBody()),HttpStatus.OK);
    }

    
    @GetMapping("/message/{jwtToken}/{email}/{role}/{login}")
    public ResponseEntity<?> messageForHeader(HttpServletRequest request, HttpServletResponse response,@PathVariable String jwtToken,@PathVariable String email,@PathVariable String role,@PathVariable Boolean login) {
        response.addHeader("Authorization", "Bearer"+jwtToken);
        
        if(login){
        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",null,null,new loginBody(role,email,login)),HttpStatus.OK);
        }else {
            return new ResponseEntity<>(new CMRespDto<>(-1,"로그인 실패",null,null,new loginBody(role,email,login)),HttpStatus.OK);
        }
    }

    //user,manager,admin 권한 접근가능
    @GetMapping("/api/v1/user")
    public String user(Authentication authentication) {
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        System.out.println("principal : "+principal.getUser().getId());
        System.out.println("principal : "+principal.getUser().getUsername());
        System.out.println("principal : "+principal.getUser().getPassword());
        return "확인";
    }

    @GetMapping("/api/v1/user/check")
    public ResponseEntity<?> userCheck(Authentication authentication) {
        System.out.println(newTokenCheck);
        PrincipalDetails principal = (PrincipalDetails) authentication.getPrincipal();
        User user= userRepository.findById(principal.getUser().getId()).orElseThrow(); 
        user.setPassword(null);
        user.setToken(null);
        return new ResponseEntity<>(new CMRespDto<>(1,"유저확인",user,null,new loginBody(null,null,true)),HttpStatus.OK);
    }

    @GetMapping("/api/v1/user/userList")
    public ResponseEntity<?> userList() {
        List<User> userList = userRepository.findAll();
        List<User> users = new ArrayList<>();
        for(User user : userList){
            user.setPassword(null);
            user.setToken(null);
            users.add(user);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"유저확인",null,users,new loginBody(null,null,true)),HttpStatus.OK);
    }
    @GetMapping("/api/v1/user/getUser/{id}")
    public ResponseEntity<?> getUser(@PathVariable int id) {
        User user = userRepository.findById(id).orElseThrow();
        if(user==null){
            return new ResponseEntity<>(new CMRespDto<>(1,"유저가 존재하지 않습니다.",null,null,new loginBody(null,null,true)),HttpStatus.OK);
        }
        user.setPassword(null);
        user.setToken(null);
        return new ResponseEntity<>(new CMRespDto<>(1,"정보 요청 성공",user,null,new loginBody(null,null,true)),HttpStatus.OK);
    }

    @PutMapping("/api/v1/user/userUpdate/{id}")
    public @ResponseBody ResponseEntity<?> userUpdate(@PathVariable int id,UserDto newUser) {
        User user = userRepository.findById(id).orElseThrow();
        if(user==null){
            return new ResponseEntity<>(new CMRespDto<>(-1,"유저가 존재하지 않습니다.",null,null,new loginBody(null,null,false)),HttpStatus.OK);
        }else if(newUser.getPassword()==null){
            newUser.setPassword(user.getPassword());
            user.Set(newUser);
            userRepository.save(user);
            return new ResponseEntity<>(new CMRespDto<>(1,"유저정보 변경완료",null,null,new loginBody(null,null,true)),HttpStatus.OK);
        }
        if(newUser.getPassword().equals(newUser.getPasswordCheck())){
            newUser.setPassword(bCryptPasswordEncoder.encode(newUser.getPasswordCheck()));
        }else{
            return new ResponseEntity<>(new CMRespDto<>(-1,"비밀번호 불일치",null,null,new loginBody(null,null,false)),HttpStatus.OK);
        }
        user.Set(newUser);
        userRepository.save(user);
        return new ResponseEntity<>(new CMRespDto<>(1,"유저정보 변경완료",null,null,new loginBody(null,null,true)),HttpStatus.OK);
    }

    @GetMapping("/exception")
    public ResponseEntity<?> exception(){
        
        System.out.println("컨트롤러 도착");
        return new ResponseEntity<>(new CMRespDto<>(-1,"인증실패",null,null,new loginBody(null,null,false)),HttpStatus.OK);
    }
    @GetMapping("/successfulLogin")
    public ResponseEntity<?> successfulLogin(){

        return new ResponseEntity<>(new CMRespDto<>(1,"로그인 성공",null,null,new loginBody()),HttpStatus.OK);

    }
    @GetMapping("/demoData/{id}/{cnf_sttus_cd}/{cumulative_sales}")
    public ResponseEntity<?> demoData(@PathVariable String id,@PathVariable int cnf_sttus_cd,@PathVariable int cumulative_sales){
        Hydrogen hydrogen = hydrogenRepository.findById(id).orElseThrow();//보내온 아이디로 수소충전소를 찾음
        int sttus_cd;//대기차량에 따라 혼잡도 설정
        if(cnf_sttus_cd<4){
            sttus_cd=1;
        }else if(cnf_sttus_cd<6){
            sttus_cd=2;
        }else{
            sttus_cd=3;
        }
        if(cumulative_sales==333){//특정값이 들어올 경우 위험도 설정
            if(sttus_cd%2==0 && hydrogen.getDangerous() <2){
                hydrogen.setSensor(hydrogen.getDangerous()+1,
                 sttus_cd+"", 
                 hydrogen.getCumulative_sales()+cumulative_sales*2, 
                 LocalDateTime.now());
            }else if(hydrogen.getDangerous() >0){
                hydrogen.setSensor(hydrogen.getDangerous()-1, 
                sttus_cd+"", 
                hydrogen.getCumulative_sales()+cumulative_sales*2, 
                LocalDateTime.now());
            }else{
                hydrogen.setSensor(hydrogen.getDangerous()+1,
                 sttus_cd+"", 
                 hydrogen.getCumulative_sales()+cumulative_sales*2, 
                 LocalDateTime.now());
            }
        }else{//아닐경우 현상유지
            hydrogen.setSensor( 
                sttus_cd+"", 
                hydrogen.getCumulative_sales()+cumulative_sales*2, 
                LocalDateTime.now());
        }
        hydrogenRepository.save(hydrogen);//설정한 값 DB에 저장
        HydrogenSensor hydrogenSensor = new HydrogenSensor(hydrogen,LocalDateTime.now(),cnf_sttus_cd+"",sttus_cd+"",cumulative_sales*2);
        hydrogenSensorRepository.save(hydrogenSensor);
        return new ResponseEntity<>(new CMRespDto<>(1,"저장성공",null,null,new loginBody()),HttpStatus.OK);
    }
    @GetMapping("/api/v1/user/getHydrogen")
    public ResponseEntity<?> getHydrogen(){
        JSONArray totelJsonArray = new JSONArray();
        try{
        List<Hydrogen> hydrogen = hydrogenRepository.findAll();
        for(int i =0;i<hydrogen.size();i++){
            JSONObject totalJsonObject = new JSONObject();
            JSONObject jSONObject = new JSONObject();
            totalJsonObject.put("id", hydrogen.get(i).getChrstn_mno());
            totalJsonObject.put("name", hydrogen.get(i).getChrstn_nm());
            jSONObject.put("lat", Double.parseDouble(hydrogen.get(i).getLet()));
            jSONObject.put("lng", Double.parseDouble(hydrogen.get(i).getLon()));
            totalJsonObject.put("position", jSONObject);
            totalJsonObject.put("operation", hydrogen.get(i).getOper_sttus_nm());
            totalJsonObject.put("congestion", hydrogen.get(i).getCnf_sttus_cd());
            totalJsonObject.put("price", hydrogen.get(i).getNtsl_pc());
            totalJsonObject.put("number", hydrogen.get(i).getChrstn_cttpc());
            totalJsonObject.put("address", hydrogen.get(i).getLotno_addr());
            totalJsonObject.put("cumulativeSales", hydrogen.get(i).getCumulative_sales());
            totelJsonArray.add(totalJsonObject);
        }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totelJsonArray,new loginBody()),HttpStatus.OK);
    }
    @GetMapping("/api/v1/user/getHydrogen2")
    public ResponseEntity<?> getHydrogen2(){
        JSONArray totelJsonArray = new JSONArray();
        try{
        List<Hydrogen> hydrogenList = hydrogenRepository.findAll();
        for(Hydrogen hydrogen : hydrogenList){
            JSONObject totalJsonObject = new JSONObject();
            totalJsonObject.put("id", hydrogen.getChrstn_mno());
            totalJsonObject.put("congestion", hydrogen.getCnf_sttus_cd());
            totalJsonObject.put("cumulativeSales", hydrogen.getCumulative_sales());
            totelJsonArray.add(totalJsonObject);
        }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totelJsonArray,new loginBody()),HttpStatus.OK);
    }
    @GetMapping("/api/v1/user/getHydrogen4")
    public ResponseEntity<?> getHydrogen5(){
        JSONArray totelJsonArray = new JSONArray();
        try{
        List<Hydrogen> hydrogenList = hydrogenRepository.findAll();
        for(Hydrogen hydrogen : hydrogenList){
            JSONObject totalJsonObject = new JSONObject();
            totalJsonObject.put("id", hydrogen.getChrstn_mno());
            totalJsonObject.put("name", hydrogen.getChrstn_nm());
            if(hydrogen.getTimestamp1().plusSeconds(5).isAfter(LocalDateTime.now())){
                totalJsonObject.put("status", hydrogen.getDangerous());
            }else{
                totalJsonObject.put("status", 3);
            }
            totelJsonArray.add(totalJsonObject);
        }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totelJsonArray,new loginBody()),HttpStatus.OK);
    }
    @GetMapping("/api/v1/user/getHydrogen3")
    public ResponseEntity<?> getHydrogen4(){
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm");
        JSONObject totalJsonObject = new JSONObject();
        totalJsonObject.put("id", "이용자수");
        totalJsonObject.put("color", "hsl(31, 70%, 50%)");
        JSONArray totelJsonArray = new JSONArray();
        LocalDateTime Date = LocalDateTime.now().plusMinutes(1);
        try{
            for(int i=15;i>0;i--){
                LocalDateTime fromDate = Date.minusMinutes(i+1);
                LocalDateTime toDate = Date.minusMinutes(i+1);
                System.out.println(fromDate.format(dateTimeFormatter));
                HydrogenSensorInterface hydrogenSensor = (hydrogenSensorRepository.findByLastMdfcnDtBetween(fromDate.format(dateTimeFormatter), toDate.format(dateTimeFormatter)));
                JSONObject JsonObject = new JSONObject();
                JsonObject.put("x", i+"분전");
                JsonObject.put("y", hydrogenSensor.getSum());
                totelJsonArray.add(JsonObject);
            }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        totalJsonObject.put("data", totelJsonArray);
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totalJsonObject,new loginBody()),HttpStatus.OK);
    }

    @GetMapping("/api/v1/user/getHydrogen2/{id}")
    public ResponseEntity<?> getHydrogen3(@PathVariable String id){
        JSONArray totelJsonArray = new JSONArray();
        try{
            
            Hydrogen hydrogen = hydrogenRepository.findById(id).orElseThrow();
            JSONObject totalJsonObject = new JSONObject();
            totalJsonObject.put("id", hydrogen.getChrstn_mno());
            totalJsonObject.put("congestion", hydrogen.getCnf_sttus_cd());
            totalJsonObject.put("cumulativeSales", hydrogen.getCumulative_sales());
            totelJsonArray.add(totalJsonObject);
        
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(1,"조회 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"조회 성공",null,totelJsonArray,new loginBody()),HttpStatus.OK);
    }
    @PostMapping("/adminmessage")
    public SocketDto<?> adminMessage(Message message){
        System.out.println("============================");

        List<Message> m = messageRepository.findByReceiverName(message.getReceiverName());
        System.out.println("============================"+m);

        return new SocketDto<>(0,0,m);      

    }
    

    //엑셀파일을 형식에 맞춰 넣으면 DB에 저장해줌
    @PostMapping("/excel/read")
    public @ResponseBody ResponseEntity<?> excelRead(BoardDto boardDto) throws IOException{
        String extension = FilenameUtils.getExtension(boardDto.getFile().get(0).getOriginalFilename());
        //파일 확장자만 저장함
        Excel excel = new Excel<>();
        
        Workbook workbook = null;
        
        if (extension.equals("xlsx")) {//보내온 확장자에 맞춰 workbook 생성
        workbook = new XSSFWorkbook(boardDto.getFile().get(0).getInputStream());
        } else if (extension.equals("xls")) {
        workbook = new HSSFWorkbook(boardDto.getFile().get(0).getInputStream());
        }else{
            return new ResponseEntity<>(new CMRespDto<>(-1,"엑셀파일만 업로드 해주세요",null,null,new loginBody()),HttpStatus.OK);
        }

        Sheet worksheet = workbook.getSheetAt(0);
        switch(worksheet.getRow(0).getCell(0).getStringCellValue()){
            case "board":
                excel.setService(boardService);
                excel.setRepository(boardRepository);
                break; 
            case "sales":
                excel.setService(salesService);
                excel.setRepository(salesRepository);
                break; 
            default: 
            return new ResponseEntity<>(new CMRespDto<>(-1,"알맞는 테이블명이 아닙니다.",null,null,new loginBody()),HttpStatus.OK);
        }
        try{
            int[] index = excel.getColumn(worksheet.getRow(1));
            excel.setTableList(new ArrayList());
            for (int i = 2; i < worksheet.getPhysicalNumberOfRows(); i++) { // 4
            Row row = worksheet.getRow(i);
            excel.save(row, index);
            }
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(-1, "엑셀 파일 읽기 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        try{
            excel.getRepository().saveAll(excel.getTableList());
        }catch(Exception e){
            return new ResponseEntity<>(new CMRespDto<>(-1,"DB 저장 실패",null,null,new loginBody()),HttpStatus.OK);
        }
        return new ResponseEntity<>(new CMRespDto<>(1,"DB 저장 성공",null,null,new loginBody()),HttpStatus.OK);
    }
}