package com.cos.testspring.repository;

import com.cos.testspring.model.file;
import org.springframework.data.jpa.repository.JpaRepository;

public interface fileRepository extends JpaRepository<file,Integer> {

}
