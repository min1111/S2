package com.cos.testspring.repository;

import com.cos.testspring.model.img;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ImgRepository extends JpaRepository<img,Integer> {

}
