package com.cos.testspring.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.cos.testspring.model.Message;



//JpaRepository를 상속했으면 어노테이션이 없어도 Ioc등록이 자동으로된다.
public interface MessageRepository extends JpaRepository<Message,Integer> {

    @Modifying
	@Query(value="select * from message;",nativeQuery=true)
	List<Message> mfind();

    // @Modifying
    // @Query(value="select count(*) from message",nativeQuery=true)
    // int count();
    long countByAlert(int alert);

    long countByAlertAndReceiverName(int alert,String receiverName);
    

    List<Message> findByAlert(int alert);

    List<Message> findByReceiverName(String receiverName);
}
