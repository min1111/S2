package com.cos.testspring.repository;

import com.cos.testspring.model.Board;
import com.cos.testspring.model.Hydrogen;
import com.cos.testspring.model.HydrogenSensor;
import com.cos.testspring.web.dto.HydrogenSensorInterface;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface HydrogenSensorRepository extends JpaRepository<HydrogenSensor,String> {

    
    @Query(value = "select SUM(wait_vhcle_alge) AS 'sum' from hydrogensensor where DATE_FORMAT(lastMdfcnDt,'%Y-%m-%d-%H-%i')"+
    " between DATE_FORMAT(NOW(),:fromDate) AND DATE_FORMAT(NOW(),:toDate);",nativeQuery=true)
    HydrogenSensorInterface findByLastMdfcnDtBetween(@Param("fromDate") String fromDate,@Param("toDate") String toDate);

    //List<HydrogenSensor> findByLastMdfcnDtBetween(LocalDateTime fromDate,LocalDateTime toDate);
}