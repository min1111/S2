package com.cos.testspring.model;

import lombok.Data;
import javax.persistence.*;

import com.cos.testspring.web.dto.UserDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Table(name="user")
@Entity
@Data
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    private String username;
    private String password;
    private String phone;
    @Column(length=100, unique = true)
    private String email;
    private String profil;
    private String role; // USER, ADMIN

    @JsonIgnoreProperties({"user"})
    @OneToMany(mappedBy="user",fetch = FetchType.EAGER) 
	private List<Token> token;

    public List<String> getRoleList(){ // 내계정이 하나유저당 롤이 두개있으면 배열의 인덱스로 뽑아서 구분짓기
        if(this.role.length()>0){
            return Arrays.asList(this.role.split(","));
        }
        return new ArrayList<>();
    }
    public void Set(UserDto userDto){
        this.username=userDto.getUsername();
        this.password=userDto.getPassword();
        this.phone=userDto.getPhone();
        this.role=userDto.getRole();
    }
    

}