package com.cos.testspring.model;



import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 번호증가전략이 데이터베이스를 따라간다.
    private int id;
    private String message;
    private String senderName;
    private String receiverName;
    private int alert;
    private Boolean toggle;

    private LocalDateTime createdAt;


	public Message toEntity(int alert) {
		return Message.builder() 
				.alert(alert)
				.build();
	}
}


