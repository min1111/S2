package com.cos.testspring.config.auth;

import com.cos.testspring.model.User;
import com.cos.testspring.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

// 시큐리티 설정에서 loginProcessingUrl("/login"); 을 걸어놔서
// /login 요청이 오면 자동으로 UserDtailsService 타입으로 IoC되어있는 loadUserByUsername함수가 실행됨
@RequiredArgsConstructor
@Service
public class PrincipalDetailsService implements UserDetailsService {

    private final UserRepository userRepository;

    // 시큐리티 session(내부Authentication(내부UserDetails))
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        System.out.println("PrincipalDetailsService의 loadUserByEmail()");
        //이메일로 인증을 하기위해 findByEmail로 변경
        User userEntity = userRepository.findByEmail(username);

        if(userEntity!=null){
            return new PrincipalDetails(userEntity); //리턴할 때 꼭 파람에 userEntity를 넣어줘야
            // PrincipalDetails 객체의 user필드에 저장되어 세션에 들어감
        }

        return null;
    }
}
